package id.ac.ui.cs.mobileprogramming.ahmad_fauzan_amirul_isnain.secretmessageservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class SecretMessageService extends Service {

    private static final String TAG = "SecretMessageService";
    public class SecretMessageServiceImpl extends ISecretMessageService.Stub
    {
        @Override
        public String getSecretMessage() throws RemoteException
        {
            Log.v(TAG, "getSecretMessage()");
            return "xxxzzxsqq1211zz";
        }
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "onCreate() called");
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.v(TAG, "onDestroy() called");
    }
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.v(TAG, "onStart() called");
    }
    @Override
    public IBinder onBind(Intent intent)
    {
        Log.v(TAG, "onBind() called");
        return new SecretMessageServiceImpl();
    }
}
// ISecretMessageService.aidl
package id.ac.ui.cs.mobileprogramming.ahmad_fauzan_amirul_isnain.secretmessageservice;

// Declare any non-default types here with import statements

interface ISecretMessageService {
    String getSecretMessage();
}
